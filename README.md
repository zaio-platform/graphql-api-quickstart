# graphql-api-quickstart

This repo is a boilerplate for starting GraphQL APIs with Apollo-Server and MongoDB

A full-fledged Apollo Server starter project with Express.

# Features of Client + Server

* Node.js with Express and Apollo Server
cursor-based Pagination
* MongoDB Database with Mongoose
entities: users, messages
* Authentication
powered by JWT and local storage
Sign Up, Sign In, Sign Out
* Authorization
protected endpoint (e.g. verify valid session)
protected resolvers (e.g. e.g. session-based)
protected routes (e.g. session-based)
* performance optimizations
example of using Facebook's dataloader
* E2E testing