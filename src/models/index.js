const Lead = require('./lead');
const ContactRequest = require('./contactRequest');

module.exports = { 
    Lead, 
    ContactRequest 
};