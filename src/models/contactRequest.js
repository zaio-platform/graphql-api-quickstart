const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const contactRequestSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name required'],
        trim: true,
    },
    contactNo: {
        type: String,
        required: [true, 'Contact number is required'],
        trim: true,
    },
	email: {
        type: String,
        unique: true,
        trim: true,
        required: [true, 'Email address is required'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,'Please fill in a valid email address'],
    },
    leadType: {
        type: String,
        required: true,
    },
    topic: {
        type: String,
        required: true,
    }
});

contactRequestSchema.plugin(timestamps);

const ContactRequest = mongoose.model('Contact', contactRequestSchema, 'Contacts');

module.exports = ContactRequest;