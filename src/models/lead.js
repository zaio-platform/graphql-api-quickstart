const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const leadSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name address is required'],
        trim: true,
    },
    contactNo: {
        type: String,
        required: [true, 'Contact number is required'],
        trim: true,
    },
	email: {
        type: String,
        unique: true,
        trim: true,
        required: [true, 'Email address is required'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,'Please fill in a valid email address'],
    },
    leadType: {
        type: String,
        required: true,
    }
});

leadSchema.plugin(timestamps);

const Lead = mongoose.model('Lead', leadSchema, 'Leads');

module.exports = Lead;