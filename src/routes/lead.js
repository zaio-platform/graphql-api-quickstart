const handlers = require('../handlers');

module.exports = function(app) {
    app.post('/api/v1/submit', handlers.submit);
    app.post('/api/v1/download', handlers.download);
    app.post('/api/v1/contact-request', handlers.contactRequest);
};