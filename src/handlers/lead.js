const mail = require('../utils/mail');
const fs = require('fs');
const appRoot = require('app-root-path');
const models = require('../models');

const submit = async (req, res ) => {
    let { name, email, leadType } = req.body;
    if (email === '' || name === '' || contactNo === '' || leadType === '', topic === '') { 
        res.send({ message: 'Please fill in all the fields.' });
        return;
    }

    // lowercase the email
    email = email.toLowerCase();
    // create the user in the database
    const lead = new models.Lead({
        name,
        email,
        contactNo,
        leadType,
    });

    try {
        await lead.save();
    } catch (error) {
        res.send({message: error.message})
        return;
    }

    // send email to sales
    const subject = "Website Lead";
    const emailData = {
        title: `Lead from ${leadType}`,
        description: `Contact ${name} within the next 24 hrs. 
                        They are interested in our client side of the platform.
                        Their is ${email}, and their contact number is ${contactNo}.`,
    };
    const sales = 'sales@zaio.io'

    try{
        await mail.sendMail(sales, subject, emailData);
    } catch (error) {
        res.send({message: error.message})
        return;
    }
    
    res.send({ message: "You'll be contacted within the next 24 hrs." });
};

const contactRequest = async (req, res ) => {
    let { name, email, contactNo, leadType, topic } = req.body;
    if (email === '' || name === '' || contactNo === '' || leadType === '', topic === '') {
        res.send({ message: 'Please fill in all the fields.' });
        return;
    }

    // lowercase the email
    email = email.toLowerCase();
    // create the user in the database
    const contactRequest = new models.ContactRequest({
        name,
        email,
        contactNo,
        leadType,
        topic
    });

    try {
        await contactRequest.save();
    } catch (error) {
        res.send({message: error.message})
        return;
    }

    // send email to sales
    const subject = "Website Lead";
    const emailData = {
        title: `Lead from ${leadType}`,
        description: `Contact ${name} about ${topic}, within the next 24 hrs. 
                        Their is ${email}, and their contact number is ${contactNo}.`,
    };
    const sales = 'sales@zaio.io'

    try{
        await mail.sendMail(sales, subject, emailData);
    } catch (error) {
        res.send({message: error.message})
        return;
    }
    
    res.send({ message: "You'll be contacted within the next 24 hrs." });
};

const download = async (req, res ) => {
    let { name, email, leadType } = req.body;
    if (email === '' || name === '' || contactNo === '' || leadType === '') {
        res.send({ message: 'Please fill in all the fields.' });
        return;
    }

    // lowercase the email
    email = email.toLowerCase();
    // create the user in the database
    const lead = new models.Lead({
        name,
        email,
        contactNo,
        leadType,
    });

    try {
        await lead.save();
    } catch (error) {
        res.send({message: error.message})
        return;
    }
    
    // Download funtionality
    let stat = fs.statSync(`${appRoot}/src/public/brochure/ProjectsPortfolio.pdf`);

    res.setHeader('Content-Length', stat.size);
    res.setHeader('Content-Type', 'application/pdf');
    await res.download(`${appRoot}/src/public/brochure/ProjectsPortfolio.pdf`);
    
    // send email to sales
    const subject = "Website Lead";
    const emailData = {
        title: `Lead from ${leadType}`,
        description: `Contact ${name} within the next 24 hrs. 
                        Their is ${email}, and their contact number is ${contactNo}.`,
    };
    const sales = 'sales@zaio.io'

    try{
        await mail.sendMail(sales, subject, emailData);
    } catch (error) {
        res.send({message: error.message})
        return;
    }

}

module.exports = {
    submit,
    contactRequest,
    download
};
