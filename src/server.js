require('dotenv-safe').config();
const express = require('express');

// Express App
const app = express();

// Middlewares
require('./config/db')();
require('./middlewares/bodyParser')(app);
require('./middlewares/cookieParser')(app);
require('./middlewares/cors')(app);
require('./middlewares/morgan')(app);
require('./middlewares/error')(app);

require('./routes/lead')(app);
module.exports = app;